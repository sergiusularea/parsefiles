import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MultipleThreads {
    private File folder;
    private HashSet<Person> persons;
    private long elapsedTime;

    public MultipleThreads(File folder, HashSet<Person> persons) {
        this.folder = folder;
        this.persons = persons;
    }

    public HashSet<Person> getPersons() {
        return persons;
    }

    public void setPersons(HashSet<Person> persons) {
        this.persons = persons;
    }

    public HashSet<Person> multipleThreadsLoadMultipleFiles() throws IOException, InterruptedException {
        HashSet<Person> persons = new HashSet<>();
        File[] fileNames = folder.listFiles();
        List<Thread> threads = new ArrayList<>();
        long startTime = System.nanoTime();
        for (File file : fileNames) {
            Thread thread = new Thread(() -> {
                try {
                    synchronized (persons) {
                        persons.addAll(Main.parseFile(file));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
            threads.add(thread);
        }
        for (Thread t : threads) {
            t.join();
        }
        elapsedTime = System.nanoTime() - startTime;
        System.out.println("Finished loading persons in " + elapsedTime / 1000000 + " miliseconds");
        return persons;
    }
}
