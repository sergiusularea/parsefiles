import java.io.File;
import java.io.IOException;
import java.util.HashSet;

public class SingleThread implements Runnable {
    private File folder;
    private HashSet<Person> persons;
    private long elapsedTime;

    public SingleThread(File folder, HashSet<Person> persons) {
        this.folder = folder;
        this.persons = persons;
    }

    public HashSet<Person> getPersons() {
        return persons;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    @Override
    public void run() {
        long startTime = System.nanoTime();
        try {
            persons = Main.parseAllFiles(folder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        long endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        System.out.println("The single thread reads all files in : " + elapsedTime / 1000000 + " miliseconds");
    }
}

