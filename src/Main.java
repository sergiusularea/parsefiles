import java.io.*;
import java.util.HashSet;


public class Main {
    static HashSet<Person> persons = new HashSet<>();
    static File folder = new File("D:\\JAVA programs\\input\\");

    public static void main(String[] args) throws InterruptedException, IOException {
//        SingleThread singleThread = new SingleThread(folder, persons);
//        Thread firstThread = new Thread(singleThread);
//        firstThread.start();
        MultipleThreads multipleThreads = new MultipleThreads(folder, persons);
        multipleThreads.multipleThreadsLoadMultipleFiles();
    }

    public static boolean checkNames(String firstName1, String firstName2, String lastName) {

        return isAlpha(firstName1) && isAlpha(firstName2) && isAlpha(lastName);
    }

    public static boolean checkCnp(String cnp) {
        boolean result = false;
        if (cnp.matches("^[0-9]{13}$") && (cnp.charAt(0) == '1' || cnp.charAt(0) == '2' || cnp.charAt(0) == '5' || cnp.charAt(0) == '6')) {
            result = true;
        }
        return result;
    }

    public static boolean checkEmail(String firstName1, String firstName2, String lastName, String email) {

        String s = firstName1 + "." + firstName2 + "." + lastName;
        return email.matches("^" + s + "+@+\\w+\\.+com");
    }

    public static boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }

    public static HashSet<Person> parseAllFiles(File folder) throws IOException {
        HashSet<Person> persons = new HashSet<>();
        File[] fileNames = folder.listFiles();
        for (File file : fileNames) {
            persons.addAll(parseFile(file));
        }
        return persons;
    }

    public static HashSet<Person> parseFile(File file) throws IOException {

        FileInputStream fstream = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        CustomStringTokenizer stringTokenizer;
        String line;
        HashSet<Person> persons = new HashSet<>();
        while ((line = br.readLine()) != null) {
            stringTokenizer = new CustomStringTokenizer(line, "#");
            System.out.println("Total persons in file " + file.getPath() + "= " + stringTokenizer.getTokensLength());
            int i = 0;
            while (i < stringTokenizer.getTokensLength()) {
                String s = stringTokenizer.getTokenAt(i);
                String[] splitOut = s.split("\\|");
                String firstName1 = splitOut[0];
                String firstName2 = splitOut[1];
                String lastName = splitOut[2];
                String cnp = splitOut[3];
                String email = splitOut[4];
                Person person = new Person(firstName1, firstName2, lastName, cnp, email);
                if (checkNames(firstName1, firstName2, lastName) && checkCnp(cnp) && checkEmail(firstName1, firstName2, lastName, email)) {
                    persons.add(person);
                }
//                else {
//                    System.out.println(person);
//                }
                i++;
            }
        }
        br.close();
        return persons;
    }
}

