import java.util.Objects;

public class Person {

    private String firstName1;
    private String firstName2;
    private String lastName;
    private String cnp;
    private String email;

    public Person(String firstName1, String firstName2, String lastName, String cnp, String email) {
        this.firstName1 = firstName1;
        this.firstName2 = firstName2;
        this.lastName = lastName;
        this.cnp = cnp;
        this.email = email;
    }

    public String getFirstName1() {
        return firstName1;
    }

    public void setFirstName1(String firstName1) {
        this.firstName1 = firstName1;
    }

    public String getFirstName2() {
        return firstName2;
    }

    public void setFirstName2(String firstName2) {
        this.firstName2 = firstName2;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName1='" + firstName1 + '\'' +
                ", firstName2='" + firstName2 + '\'' +
                ", lastName='" + lastName + '\'' +
                ", cnp='" + cnp + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName1, person.firstName1) &&
                Objects.equals(firstName2, person.firstName2) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(cnp, person.cnp) &&
                Objects.equals(email, person.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName1, firstName2, lastName, cnp, email);
    }
}
